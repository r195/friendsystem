package net.ravenix.friendsystem.bungee.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import net.ravenix.friendsystem.bungee.BungeeFriends;
import net.ravenix.friendsystem.shared.friend.IFriendProvider;

public final class MSGCommand extends Command {

    public MSGCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;
        IFriendProvider friendProvider = BungeeFriends.getInstance().getFriendProvider();
        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
        ICorePlayerProvider corePlayerProvider = BungeeCore.getInstance().getCorePlayerProvider();

        if (args.length < 2) {
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§7Nutze§8: §b/msg <Name> <Nachricht>");
            return;
        }
        String playerName = args[0];
        NameResult nameResultByName = nameStorageProvider.getNameResultByName(playerName);
        if (nameResultByName == null) {
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDieser Spieler existiert nicht.");
            return;
        }
        if (!friendProvider.isFriend(proxiedPlayer.getUniqueId(), nameResultByName.getUuid())) {
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu bist nicht mit diesem Spieler befreudet.");
            return;
        }
        if (corePlayerProvider.getCorePlayer(nameResultByName.getUuid()) == null) {
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDieser Spieler ist nicht online.");
            return;
        }
        int current = 0;
        int max = args.length;
        StringBuilder stringBuilder = new StringBuilder();
        for (String message : args) {
            if (!message.equalsIgnoreCase(nameResultByName.getName())) {
                if (current == max) {
                    stringBuilder.append(message);
                } else {
                    stringBuilder.append(message).append(" ");
                }
            }
            current++;
        }
        String message = stringBuilder.toString();
        friendProvider.sendPrivateMessage(proxiedPlayer.getUniqueId(), nameResultByName.getUuid(), message, true);
    }
}
