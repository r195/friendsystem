package net.ravenix.friendsystem.bungee;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.bungee.mysql.MySQL;
import net.ravenix.friendsystem.bungee.commands.FriendCommand;
import net.ravenix.friendsystem.bungee.commands.MSGCommand;
import net.ravenix.friendsystem.bungee.commands.RCommand;
import net.ravenix.friendsystem.bungee.friend.FriendProvider;
import net.ravenix.friendsystem.bungee.listener.PlayerJoinQuitListener;
import net.ravenix.friendsystem.bungee.listener.PubSubListener;
import net.ravenix.friendsystem.shared.friend.IFriendProvider;

public final class BungeeFriends extends Plugin {

    @Getter
    private static BungeeFriends instance;

    @Getter
    private final String prefix = "§8[§a§lF§2§lreunde§8] §7";

    @Getter
    private MySQL mySQL;

    @Getter
    private IFriendProvider friendProvider;

    @Override
    public void onEnable() {
        instance = this;

        this.mySQL = BungeeCore.getInstance().getMySQL();
        initTables();

        this.friendProvider = new FriendProvider(this.mySQL);

        loadCommandsAndListeners();
    }

    @Override
    public void onDisable() {

    }

    private void loadCommandsAndListeners() {
        CloudNetDriver.getInstance().getEventManager().registerListener(new PubSubListener());

        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();
        pluginManager.registerListener(this, new PlayerJoinQuitListener());
        pluginManager.registerCommand(this, new FriendCommand("friend"));
        pluginManager.registerCommand(this, new MSGCommand("msg"));
        pluginManager.registerCommand(this, new RCommand("r"));
    }

    private void initTables() {
        this.mySQL.update("CREATE TABLE IF NOT EXISTS friends (uuid varchar(36), friendUUID varchar(36))");
        this.mySQL.update("CREATE TABLE IF NOT EXISTS requests (uuid varchar(36), request varchar(36))");
    }
}
