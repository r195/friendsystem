package net.ravenix.friendsystem.bungee.commands;

import com.google.common.collect.Lists;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.player.ICorePlayer;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import net.ravenix.friendsystem.bungee.BungeeFriends;
import net.ravenix.friendsystem.shared.friend.IFriendProvider;

import java.util.List;
import java.util.UUID;

public final class FriendCommand extends Command {

    public FriendCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;
        if (args.length == 0) {
            sendHelp(proxiedPlayer, 1);
            return;
        }
        IFriendProvider friendProvider = BungeeFriends.getInstance().getFriendProvider();
        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();
        ICorePlayerProvider corePlayerProvider = BungeeCore.getInstance().getCorePlayerProvider();
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("denyAll")) {
                List<UUID> requests = friendProvider.getRequests(proxiedPlayer.getUniqueId());
                if (requests == null || requests.size() == 0) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu hast keine offenen Freundschaftsanfragen.");
                    return;
                }
                requests.forEach(requestUUID -> {
                    String command = "friend deny " + nameStorageProvider.getNameResultByUUID(requestUUID).getName();
                    executeCommand(proxiedPlayer.getUniqueId(), command);
                });
                return;
            }
            if (args[0].equalsIgnoreCase("acceptAll")) {
                List<UUID> requests = friendProvider.getRequests(proxiedPlayer.getUniqueId());
                if (requests == null || requests.size() == 0) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu hast keine offenen Freundschaftsanfragen.");
                    return;
                }
                requests.forEach(requestUUID -> {
                    String command = "friend accept " + nameStorageProvider.getNameResultByUUID(requestUUID).getName();
                    executeCommand(proxiedPlayer.getUniqueId(), command);
                });
                return;
            }
            if (args[0].equalsIgnoreCase("party")) {
                List<UUID> friends = friendProvider.getFriends(proxiedPlayer.getUniqueId());
                if (friends == null || friends.size() == 0) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu hast keine Freunde.");
                    return;
                }
                List<UUID> onlineFriends = Lists.newArrayList();
                friends.forEach(friendUUID -> {
                    if (corePlayerProvider.getCorePlayer(friendUUID) != null)
                        onlineFriends.add(friendUUID);
                });

                if (onlineFriends.size() == 0) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDerzeit ist keiner deiner Freunde online.");
                    return;
                }

                onlineFriends.forEach(friendUUID -> {
                    String command = "party invite " + nameStorageProvider.getNameResultByUUID(friendUUID).getName();
                    executeCommand(proxiedPlayer.getUniqueId(), command);
                });
                return;
            }
            if (args[0].equalsIgnoreCase("removeAll")) {
                List<UUID> friends = friendProvider.getFriends(proxiedPlayer.getUniqueId());
                if (friends == null || friends.size() == 0) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu hast keine Freunde.");
                    return;
                }
                friends.forEach(friendUUID -> {
                    String command = "friend remove " + nameStorageProvider.getNameResultByUUID(friendUUID).getName();
                    executeCommand(proxiedPlayer.getUniqueId(), command);
                });
                return;
            }
            if (args[0].equalsIgnoreCase("requests")) {
                List<UUID> requests = friendProvider.getRequests(proxiedPlayer.getUniqueId());
                if (requests == null || requests.size() == 0) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu hast keine offenen Freundschaftsanfragen.");
                    return;
                }
                requests.forEach(requestUUID -> {
                    NameResult nameResult = nameStorageProvider.getNameResultByUUID(requestUUID);
                    IPermissionUser permissionUser = permissionProvider.getPermissionUser(requestUUID);
                    proxiedPlayer.sendMessage("§8- " + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName());
                });
                if (requests.size() == 1) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§7Du hast §aeine §7offene Freundschaftsanfrage.");
                 return;
                }
                proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§7Du hast §a" + requests.size() + " §7offene Freundschaftsanfragen.");
                return;
            }
            if (args[0].equalsIgnoreCase("list")) {
                List<UUID> friends = friendProvider.getFriends(proxiedPlayer.getUniqueId());
                if (friends == null) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu hast keine Freunde.");
                    return;
                }
                List<UUID> onlineFriends = Lists.newArrayList();
                friends.forEach(friendUUID -> {
                    if (corePlayerProvider.getCorePlayer(friendUUID) != null)
                        onlineFriends.add(friendUUID);
                });

                onlineFriends.forEach(friendUUID -> {
                    NameResult nameResultByUUID = nameStorageProvider.getNameResultByUUID(friendUUID);
                    IPermissionUser permissionUser = permissionProvider.getPermissionUser(friendUUID);
                    ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(friendUUID);
                    proxiedPlayer.sendMessage(
                            BungeeFriends.getInstance().getPrefix()
                                    + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY)
                                    + nameResultByUUID.getName() + " §7online auf §b" + corePlayer.getServer());
                });
                if (onlineFriends.size() == 0) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDerzeit ist keiner deiner Freunde online.");
                    return;
                }
                if (onlineFriends.size() == 1) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§7Es ist derzeit §aein §7Freund online.");
                    return;
                }
                proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§7Es sind derzeit §a" + onlineFriends.size() + " §7Freunde online.");
                return;
            }
        }
        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("help")) {
                int page;
                try {
                    page = Integer.parseInt(args[1]);
                } catch (NumberFormatException ignored) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cBitte gebe eine Zahl an.");
                    return;
                }
                sendHelp(proxiedPlayer, page);
                return;
            }
            String playerName = args[1];
            NameResult nameResult = nameStorageProvider.getNameResultByName(playerName);
            if (nameResult == null) {
                proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDieser Spieler existiert nicht.");
                return;
            }
            if (args[0].equalsIgnoreCase("add")) {
                if (nameResult.getUuid().equals(proxiedPlayer.getUniqueId())) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu kannst dich nicht selber als Freund hinzufügen.");
                    return;
                }
                if (friendProvider.isFriend(proxiedPlayer.getUniqueId(), nameResult.getUuid())) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu bist bereits mit diesem Spieler befreundet.");
                    return;
                }
                if (friendProvider.hasRequests(proxiedPlayer.getUniqueId(), nameResult.getUuid())) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu hast diesem Spieler bereits eine Anfrage geschickt.");
                    return;
                }
                friendProvider.sendRequest(proxiedPlayer.getUniqueId(), nameResult.getUuid(), true);
                return;
            }
            if (args[0].equalsIgnoreCase("jump")) {
                if (!friendProvider.isFriend(proxiedPlayer.getUniqueId(), nameResult.getUuid())) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu bist nicht mit diesem Spieler befreundet.");
                    return;
                }
                ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(nameResult.getUuid());
                if (corePlayer == null) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDieser Spieler ist nicht online.");
                    return;
                }
                ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo(corePlayer.getServer());
                proxiedPlayer.connect(serverInfo);
                IPermissionUser permissionUser = permissionProvider.getPermissionUser(nameResult.getUuid());
                proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§7Du bist "
                        + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §7auf den Server §b" + serverInfo.getName() + " §7hinterhergespruungen.");
                return;
            }
            if (args[0].equalsIgnoreCase("deny")) {
                if (!friendProvider.hasRequests(nameResult.getUuid(), proxiedPlayer.getUniqueId())) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu hast keine Anfrage von diesem Spieler.");
                    return;
                }
                friendProvider.denyRequest(proxiedPlayer.getUniqueId(), nameResult.getUuid(), true);
                return;
            }
            if (args[0].equalsIgnoreCase("accept")) {
                if (friendProvider.isFriend(proxiedPlayer.getUniqueId(), nameResult.getUuid())) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu bist bereits mit diesem Spieler befreundet.");
                    return;
                }
                if (!friendProvider.hasRequests(nameResult.getUuid(), proxiedPlayer.getUniqueId())) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu hast keine Anfrage von diesem Spieler.");
                    return;
                }
                friendProvider.addFriend(proxiedPlayer.getUniqueId(), nameResult.getUuid(), true);
                return;
            }
            if (args[0].equalsIgnoreCase("remove")) {
                if (!friendProvider.isFriend(proxiedPlayer.getUniqueId(), nameResult.getUuid())) {
                    proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu bist nicht mit diesem Spieler befreundet.");
                    return;
                }
                friendProvider.removeFriend(proxiedPlayer.getUniqueId(), nameResult.getUuid(), true);
                return;
            }
        }


        sendHelp(proxiedPlayer, 1);
    }

    private void sendHelp(ProxiedPlayer proxiedPlayer, int page) {
        if (page == 2) {
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§aFreundeSystem §8| §72 Seite");
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§a/friend denyAll");
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§a/friend acceptAll");
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§a/friend removeAll");
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§a/friend party");
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§a/friend list");
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§a/msg");
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§a/r");
        } else {
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§aFreundeSystem §8| §71 Seite");
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§a/friend help <Seite>");
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§a/friend add <Name>");
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§a/friend accept <Name>");
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§a/friend deny <Name>");
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§a/friend remove <Name>");
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§a/friend jump <Name>");
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§a/friend requests");
        }
    }

    private void executeCommand(UUID uuid, String command) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid);
        jsonDocument.append("command", command);
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("executeCommand", "update", jsonDocument);
    }
}
