package net.ravenix.friendsystem.bungee.listener;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.event.EventListener;
import de.dytanic.cloudnet.driver.event.events.channel.ChannelMessageReceiveEvent;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.friendsystem.bungee.BungeeFriends;
import net.ravenix.friendsystem.shared.friend.IFriendProvider;

import java.util.UUID;

public final class PubSubListener {

    @EventListener
    public void channelMessage(ChannelMessageReceiveEvent event) {
        JsonDocument jsonDocument = event.getData();
        IFriendProvider friendProvider = BungeeFriends.getInstance().getFriendProvider();
        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();
        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
        if (event.getChannel().equalsIgnoreCase("privateMessage")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID friendUUID = UUID.fromString(jsonDocument.getString("friendUUID"));
            String message = jsonDocument.getString("message");

            NameResult nameResult = nameStorageProvider.getNameResultByUUID(uuid);
            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);

            NameResult friendNameResult = nameStorageProvider.getNameResultByUUID(friendUUID);
            IPermissionUser friendPermissionUser = permissionProvider.getPermissionUser(friendUUID);

            ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
            if (player != null)
                player.sendMessage(BungeeFriends.getInstance().getPrefix() + "§8[" + permissionUser.getHighestGroup().getColor() + "Du §8-> "
                        + friendPermissionUser.getHighestGroup().getColor() + friendNameResult.getName() + "§8] §7" + message);

            ProxiedPlayer friendPlayer = ProxyServer.getInstance().getPlayer(friendUUID);
            if (friendPlayer != null)
                friendPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§8["
                        + permissionUser.getHighestGroup().getColor()
                        + nameResult.getName() + " §8-> " + friendPermissionUser.getHighestGroup().getColor() + "Dir§8] §7" + message);

            friendProvider.sendPrivateMessage(uuid, friendUUID, message, false);
        }

        if (event.getChannel().equalsIgnoreCase("friendJoin")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            NameResult nameResultByUUID = nameStorageProvider.getNameResultByUUID(uuid);
            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);

            if (friendProvider.getFriends(uuid) == null) return;

            friendProvider.getFriends(uuid).forEach(friendUUID -> {
                ProxiedPlayer player = ProxyServer.getInstance().getPlayer(friendUUID);
                if (player != null)
                    player.sendMessage(BungeeFriends.getInstance().getPrefix()
                            + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResultByUUID.getName() + " §7ist nun §aonline§7.");
            });

        }
        if (event.getChannel().equalsIgnoreCase("friendQuit")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            NameResult nameResultByUUID = nameStorageProvider.getNameResultByUUID(uuid);
            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);

            if (friendProvider.getFriends(uuid) == null) return;

            friendProvider.getFriends(uuid).forEach(friendUUID -> {
                ProxiedPlayer player = ProxyServer.getInstance().getPlayer(friendUUID);
                if (player != null)
                    player.sendMessage(BungeeFriends.getInstance().getPrefix()
                            + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResultByUUID.getName() + " §7ist nun §coffline§7.");
            });

        }
        if (event.getChannel().equalsIgnoreCase("addFriend")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID friendUUID = UUID.fromString(jsonDocument.getString("friendUUID"));

            NameResult nameResult = nameStorageProvider.getNameResultByUUID(uuid);
            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);

            NameResult friendNameResult = nameStorageProvider.getNameResultByUUID(friendUUID);
            IPermissionUser friendPermissionUser = permissionProvider.getPermissionUser(friendUUID);

            ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
            if (player != null)
                player.sendMessage(BungeeFriends.getInstance().getPrefix() + "§7Du bist nun mit "
                        + friendPermissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + friendNameResult.getName() + " §7befreundet.");

            ProxiedPlayer friendPlayer = ProxyServer.getInstance().getPlayer(friendUUID);
            if (friendPlayer != null)
                friendPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§7Du bist nun mit "
                        + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §7befreundet.");

            friendProvider.addFriend(uuid, friendUUID, false);
        }
        if (event.getChannel().equalsIgnoreCase("removeFriend")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID friendUUID = UUID.fromString(jsonDocument.getString("friendUUID"));

            NameResult nameResult = nameStorageProvider.getNameResultByUUID(uuid);
            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);

            NameResult friendNameResult = nameStorageProvider.getNameResultByUUID(friendUUID);
            IPermissionUser friendPermissionUser = permissionProvider.getPermissionUser(friendUUID);

            ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
            if (player != null)
                player.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDie Freundschaft mit "
                        + friendPermissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + friendNameResult.getName() + " §cwurde aufgelöst.");

            ProxiedPlayer friendPlayer = ProxyServer.getInstance().getPlayer(friendUUID);
            if (friendPlayer != null)
                friendPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDie Freundschaft mit "
                        + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §cwurde aufgelöst.");

            friendProvider.removeFriend(uuid, friendUUID, false);
        }
        if (event.getChannel().equalsIgnoreCase("addRequest")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID requestUUID = UUID.fromString(jsonDocument.getString("requestUUID"));

            NameResult nameResult = nameStorageProvider.getNameResultByUUID(uuid);
            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);

            NameResult friendNameResult = nameStorageProvider.getNameResultByUUID(requestUUID);
            IPermissionUser friendPermissionUser = permissionProvider.getPermissionUser(requestUUID);

            ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
            if (player != null)
                player.sendMessage(BungeeFriends.getInstance().getPrefix() + "§7Du hast "
                        + friendPermissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + friendNameResult.getName() + " §7eine Freundschaftsanfrage gesendet.");

            ProxiedPlayer friendPlayer = ProxyServer.getInstance().getPlayer(requestUUID);
            if (friendPlayer != null) {
                friendPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§7Du hast eine Freundschaftsanfrage von "
                        + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §7erhalten.");
                TextComponent accept = new TextComponent();
                accept.setText(BungeeFriends.getInstance().getPrefix() + "§a§l[ANNEHMEN]");
                accept.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend accept " + nameResult.getName()));

                TextComponent deny = new TextComponent();
                deny.setText(BungeeFriends.getInstance().getPrefix() + "§c§l[ABLEHNEN]");
                deny.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend deny " + nameResult.getName()));

                friendPlayer.sendMessage(accept);
                friendPlayer.sendMessage(deny);
            }

            friendProvider.sendRequest(uuid, requestUUID, false);
        }
        if (event.getChannel().equalsIgnoreCase("denyRequest")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID requestUUID = UUID.fromString(jsonDocument.getString("requestUUID"));

            NameResult nameResult = nameStorageProvider.getNameResultByUUID(uuid);
            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);

            NameResult friendNameResult = nameStorageProvider.getNameResultByUUID(requestUUID);
            IPermissionUser friendPermissionUser = permissionProvider.getPermissionUser(requestUUID);

            ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
            if (player != null)
                player.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu hast die Freundschaftsanfrage von "
                        + friendPermissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY)
                        + friendNameResult.getName() + " §cabgelehnt.");

            ProxiedPlayer friendPlayer = ProxyServer.getInstance().getPlayer(requestUUID);
            if (friendPlayer != null)
                friendPlayer.sendMessage(BungeeFriends.getInstance().getPrefix()
                        + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY)
                        + nameResult.getName() + " §chat deine Freundschaftsanfrage abgelehnt.");

            friendProvider.denyRequest(uuid, requestUUID, false);
        }
    }

}
