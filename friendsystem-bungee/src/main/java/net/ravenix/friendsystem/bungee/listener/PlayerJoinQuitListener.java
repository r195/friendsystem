package net.ravenix.friendsystem.bungee.listener;

import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.ravenix.friendsystem.bungee.BungeeFriends;
import net.ravenix.friendsystem.shared.friend.IFriendProvider;

import java.util.List;
import java.util.UUID;

public final class PlayerJoinQuitListener
        implements Listener {

    @EventHandler
    public void postLoginEvnet(PostLoginEvent event) {
        IFriendProvider friendProvider = BungeeFriends.getInstance().getFriendProvider();
        friendProvider.friendJoinCall(event.getPlayer().getUniqueId());

        List<UUID> requests = friendProvider.getRequests(event.getPlayer().getUniqueId());
        if (requests == null) return;
        if (requests.size() > 0) {
            if (requests.size() == 1) {
                event.getPlayer().sendMessage(BungeeFriends.getInstance().getPrefix() + "§7Du hast §aeine §7offene Freundschaftsanfrage.");
            } else {
                event.getPlayer().sendMessage(BungeeFriends.getInstance().getPrefix() + "§7Du hast §a" + requests.size() + " §7offene Freundschaftsanfrage.");
            }
        }

    }

    @EventHandler
    public void playerDisconnect(PlayerDisconnectEvent event) {
        BungeeFriends.getInstance().getFriendProvider().friendLeaveCall(event.getPlayer().getUniqueId());
    }

}
