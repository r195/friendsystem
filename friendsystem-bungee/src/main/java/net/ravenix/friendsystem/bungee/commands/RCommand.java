package net.ravenix.friendsystem.bungee.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import net.ravenix.friendsystem.bungee.BungeeFriends;
import net.ravenix.friendsystem.shared.friend.IFriendProvider;

import java.util.UUID;

public final class RCommand extends Command {

    public RCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer)commandSender;
        IFriendProvider friendProvider = BungeeFriends.getInstance().getFriendProvider();
        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
        ICorePlayerProvider corePlayerProvider = BungeeCore.getInstance().getCorePlayerProvider();

        if (args.length == 0) {
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§7Nutze§8: §b/r <Nachricht>");
            return;
        }
        UUID lastMessager = friendProvider.getLastMessager(proxiedPlayer.getUniqueId());
        if (lastMessager == null) {
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDu hast bisher keine Nachricht von eimem Freund erhalten.");
            return;
        }
        if (corePlayerProvider.getCorePlayer(lastMessager) == null) {
            proxiedPlayer.sendMessage(BungeeFriends.getInstance().getPrefix() + "§cDieser Spieler ist nicht online.");
            return;
        }
        int current = 0;
        int max = args.length - 1;
        StringBuilder stringBuilder = new StringBuilder();
        for (String message : args) {
            if (current == max) {
                stringBuilder.append(message);
            } else {
                stringBuilder.append(message).append(" ");
            }
            current++;
        }
        String message = stringBuilder.toString();
        friendProvider.sendPrivateMessage(proxiedPlayer.getUniqueId(), lastMessager, message, true);
    }
}
