package net.ravenix.friendsystem.bukkit.listener;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.event.EventListener;
import de.dytanic.cloudnet.driver.event.events.channel.ChannelMessageReceiveEvent;
import net.ravenix.friendsystem.bukkit.BukkitFriends;
import net.ravenix.friendsystem.shared.friend.IFriendProvider;

import java.util.UUID;

public final class PubSubListener {

    @EventListener
    public void channelMessage(ChannelMessageReceiveEvent event) {
        JsonDocument jsonDocument = event.getData();
        IFriendProvider friendProvider = BukkitFriends.getInstance().getFriendProvider();

        if (event.getChannel().equalsIgnoreCase("addFriend")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID friendUUID = UUID.fromString(jsonDocument.getString("friendUUID"));

            friendProvider.addFriend(uuid, friendUUID, false);
        }
        if (event.getChannel().equalsIgnoreCase("removeFriend")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID friendUUID = UUID.fromString(jsonDocument.getString("friendUUID"));

            friendProvider.removeFriend(uuid, friendUUID, false);
        }
        if (event.getChannel().equalsIgnoreCase("addRequest")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID requestUUID = UUID.fromString(jsonDocument.getString("requestUUID"));

            friendProvider.sendRequest(uuid, requestUUID, false);
        }
        if (event.getChannel().equalsIgnoreCase("denyRequest")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID requestUUID = UUID.fromString(jsonDocument.getString("requestUUID"));

            friendProvider.denyRequest(uuid, requestUUID, false);
        }
    }

}

