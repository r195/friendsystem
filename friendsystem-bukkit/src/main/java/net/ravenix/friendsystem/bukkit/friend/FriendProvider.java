package net.ravenix.friendsystem.bukkit.friend;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.ravenix.core.bukkit.mysql.MySQL;
import net.ravenix.friendsystem.shared.friend.IFriendProvider;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public final class FriendProvider implements IFriendProvider {

    private final MySQL mySQL;

    private final Map<UUID, List<UUID>> friendsMaps = Maps.newHashMap();

    private final Map<UUID, List<UUID>> requestsMap = Maps.newHashMap();

    private final Map<UUID, UUID> lastMessager = Maps.newHashMap();

    public FriendProvider(MySQL mySQL) {
        this.mySQL = mySQL;

        loadFriends();
        loadRequests();
    }

    @Override
    public void addFriend(UUID uuid, UUID friendUUID, boolean send) {
        List<UUID> friendPlayerFriends = this.friendsMaps.get(friendUUID);
        if (friendPlayerFriends != null) {
            if (!friendPlayerFriends.contains(uuid))
                friendPlayerFriends.add(uuid);
        } else {
            List<UUID> friends = Lists.newArrayList();
            friends.add(uuid);
            this.friendsMaps.put(friendUUID, friends);
        }

        List<UUID> playerFriends = this.friendsMaps.get(uuid);
        if (playerFriends != null) {
            if (!playerFriends.contains(friendUUID))
                playerFriends.add(friendUUID);
        } else {
            List<UUID> friends = Lists.newArrayList();
            friends.add(friendUUID);
            this.friendsMaps.put(uuid, friends);
        }
        List<UUID> uuids = this.requestsMap.get(friendUUID);
        if (uuids != null)
            uuids.remove(uuid);


        List<UUID> requests = this.requestsMap.get(uuid);
        if (requests != null)
            requests.remove(friendUUID);

    }

    @Override
    public void removeFriend(UUID uuid, UUID friendUUID, boolean send) {
        List<UUID> friends = getFriends(uuid);
        friends.remove(friendUUID);

        List<UUID> friendFriends = getFriends(friendUUID);
        friendFriends.remove(uuid);

    }

    @Override
    public boolean isFriend(UUID uuid, UUID friendUUID) {
        List<UUID> friends = this.friendsMaps.get(friendUUID);
        if (friends == null) return false;
        return friends.contains(uuid);
    }

    @Override
    public void sendRequest(UUID uuid, UUID requestUUID, boolean send) {
        List<UUID> requests = this.requestsMap.get(requestUUID);
        if (requests != null) {
            if (!requests.contains(uuid))
                requests.add(uuid);
        } else {
            List<UUID> requestsList = Lists.newArrayList();
            requestsList.add(uuid);
            this.requestsMap.put(requestUUID, requestsList);
        }
    }

    @Override
    public boolean hasRequests(UUID uuid, UUID requestUUID) {
        List<UUID> requests = this.requestsMap.get(requestUUID);
        if (requests == null) return false;
        return requests.contains(uuid);
    }

    @Override
    public void denyRequest(UUID uuid, UUID requestUUID, boolean send) {
        List<UUID> requests = this.requestsMap.get(uuid);
        requests.remove(requestUUID);

    }

    @Override
    public void friendJoinCall(UUID uuid) {

    }

    @Override
    public void friendLeaveCall(UUID uuid) {

    }

    @Override
    public List<UUID> getRequests(UUID uuid) {
        return this.requestsMap.get(uuid);
    }

    @Override
    public List<UUID> getFriends(UUID uuid) {
        return this.friendsMaps.get(uuid);
    }

    @Override
    public void sendPrivateMessage(UUID uuid, UUID friendUUID, String message, boolean send) {

    }

    @Override
    public UUID getLastMessager(UUID uuid) {
        return this.lastMessager.get(uuid);
    }

    @Override
    public void setLastMessager(UUID uuid, UUID lastMessager) {
        this.lastMessager.remove(uuid);
        this.lastMessager.put(uuid, lastMessager);
    }

    private void loadRequests() {
        ResultSet resultSet = this.mySQL.getResult("SELECT * FROM requests");
        while (true) {
            try {
                if (!resultSet.next()) break;
                UUID uuid = UUID.fromString(resultSet.getString("uuid"));
                UUID request = UUID.fromString(resultSet.getString("request"));

                if (this.requestsMap.containsKey(uuid)) {
                    List<UUID> requests = this.requestsMap.get(uuid);
                    if (!requests.contains(request))
                        requests.add(request);
                } else {
                    List<UUID> requests = Lists.newArrayList();
                    requests.add(request);
                    this.requestsMap.put(uuid, requests);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadFriends() {
        ResultSet resultSet = this.mySQL.getResult("SELECT * FROM friends");
        while (true) {
            try {
                if (!resultSet.next()) break;

                UUID uuid = UUID.fromString(resultSet.getString("uuid"));
                UUID friendUUID = UUID.fromString(resultSet.getString("friendUUID"));

                if (this.friendsMaps.containsKey(uuid)) {
                    List<UUID> friends = this.friendsMaps.get(uuid);
                    if (!friends.contains(friendUUID))
                        friends.add(friendUUID);
                } else {
                    List<UUID> friends = Lists.newArrayList();
                    friends.add(friendUUID);
                    this.friendsMaps.put(uuid, friends);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
