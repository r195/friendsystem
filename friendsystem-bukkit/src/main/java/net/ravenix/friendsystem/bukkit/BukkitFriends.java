package net.ravenix.friendsystem.bukkit;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import lombok.Getter;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.friendsystem.bukkit.friend.FriendProvider;
import net.ravenix.friendsystem.bukkit.listener.PubSubListener;
import net.ravenix.friendsystem.shared.friend.IFriendProvider;
import org.bukkit.plugin.java.JavaPlugin;

public final class BukkitFriends extends JavaPlugin {

    @Getter
    private static BukkitFriends instance;

    @Getter
    private IFriendProvider friendProvider;

    @Override
    public void onEnable() {
        instance = this;

        this.friendProvider = new FriendProvider(BukkitCore.getInstance().getMySQL());

        CloudNetDriver.getInstance().getEventManager().registerListener(new PubSubListener());
    }

    @Override
    public void onDisable() {

    }
}
