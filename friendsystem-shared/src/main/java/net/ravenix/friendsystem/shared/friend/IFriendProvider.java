package net.ravenix.friendsystem.shared.friend;

import java.util.List;
import java.util.UUID;

public interface IFriendProvider {

    void addFriend(UUID uuid, UUID friendUUID, boolean send);

    void removeFriend(UUID uuid, UUID friendUUID, boolean send);

    boolean isFriend(UUID uuid, UUID friendUUID);

    void sendRequest(UUID uuid, UUID requestUUID, boolean send);

    boolean hasRequests(UUID uuid, UUID requestUUID);

    void denyRequest(UUID uuid, UUID requestUUID, boolean send);

    void friendJoinCall(UUID uuid);

    void friendLeaveCall(UUID uuid);

    List<UUID> getRequests(UUID uuid);

    List<UUID> getFriends(UUID uuid);

    void sendPrivateMessage(UUID uuid, UUID friendUUID, String message, boolean send);

    UUID getLastMessager(UUID uuid);

    void setLastMessager(UUID uuid, UUID lastMessager);

}
